<?php
/**
 * @file
 * syclops_grants.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function syclops_grants_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-syclops_grant-block_1'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_grant-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_grant-block_2'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_grant-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_grant-block_3'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_grant-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  $export['views-syclops_summary-block_4'] = array(
    'cache' => -1,
    'custom' => '0',
    'delta' => 'syclops_summary-block_4',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'syclops' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'syclops',
        'weight' => '0',
      ),
    ),
    'title' => '',
    'visibility' => '0',
  );

  return $export;
}

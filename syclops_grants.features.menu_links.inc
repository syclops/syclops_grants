<?php
/**
 * @file
 * syclops_grants.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function syclops_grants_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation:node/add/grant
  $menu_links['navigation:node/add/grant'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/grant',
    'router_path' => 'node/add/grant',
    'link_title' => 'Grant',
    'options' => array(
      'attributes' => array(
        'title' => 'Allows or Denies access to server machine.',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Grant');


  return $menu_links;
}

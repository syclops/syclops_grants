<?php
/**
 * @file
 * syclops_grants.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function syclops_grants_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'grant';
  $context->description = '';
  $context->tag = 'User';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'grant' => 'grant',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
        'views-syclops_activities-block_7' => array(
          'module' => 'views',
          'delta' => 'syclops_activities-block_7',
          'region' => 'content',
          'weight' => '-10',
        ),
        'block-5' => array(
          'module' => 'block',
          'delta' => '5',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-syclops_grant-block_3' => array(
          'module' => 'views',
          'delta' => 'syclops_grant-block_3',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('User');
  $export['grant'] = $context;

  return $export;
}
